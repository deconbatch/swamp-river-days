/**
 * Swamp River Days.
 * one of the vector field pattern with perlin noise.
 * 
 * @author @deconbatch
 * @version 0.1
 * Processing 3.2.1
 * 2019.05.19
 */

void setup() {

  size(1080, 1080);
  colorMode(HSB, 360, 100, 100, 100);
  smooth();
  noLoop();

}

void draw() {

  noiseSeed(floor(random(100)));

  // shape
  float plotDiv = 0.001;
  float initDiv = 0.01;
  int   plotMax = 20000;

  // color
  float baseHue = random(360.0);
  float baseSat = 60.0;
  float baseBri = 70.0;
  float baseAlp = 10.0;
  float baseSiz = 1.0;

  background(0.0, 0.0, 90.0, 100);
  noStroke();

  // initial points makes squre shape
  float xDiv = initDiv;
  float yDiv = initDiv;
  for (float xInit = 0.0; xInit <= 1.0; xInit += xDiv) {
    xDiv = map(sin(PI * xInit), 0.0, 1.0, initDiv * 5.0, initDiv);

    for (float yInit = 0.0; yInit <= 1.0; yInit += yDiv) {
      yDiv = map(sin(PI * yInit), 0.0, 1.0, initDiv * 3.0, initDiv);

      int   sparse = floor((xInit + yInit) * 100.0) % 4;  // make 0, 1, 2, 3 with initial point

      // draw vector field
      float xPoint = xInit;
      float yPoint = yInit;
      float rPoint = 0.0;
      for (int plotCnt = 0; plotCnt < plotMax; ++plotCnt) {

        float pRatio = map(plotCnt, 0, plotMax, 0.0, 1.0);

        // vector field calculation
        float xPrev = xPoint;
        float yPrev = yPoint;
        float rPrev = rPoint;
        rPoint += map(noise(xPrev * 10.0, yPrev * 10.0), 0.0, 1.0, -1.0, 1.0);
        xPoint += plotDiv * cos(TWO_PI * rPoint) * pRatio;
        yPoint += plotDiv * sin(TWO_PI * rPoint) * pRatio;

        float pHue   = baseHue + pRatio * 60.0 + sparse * 10.0;
        float pSat   = baseSat * map(sin(PI * pRatio), 0.0, 1.0, 1.0, 0.1);
        float pBri   = baseBri * sin(PI * pRatio) * (sparse + 6.0) / 9.0;
        float pSiz   = baseSiz * sin(PI * pRatio);
        fill(pHue % 360.0, pSat, pBri, baseAlp);
        ellipse(xPoint * width, yPoint * height, pSiz, pSiz);

      }
    }
  }
  
  casing();
  saveFrame("frames/####.png");
  exit();

}

/**
 * casing : draw fancy casing
 */
private void casing() {
  blendMode(BLEND);
  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(60.0);
  stroke(0.0, 0.0, 0.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(50.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  rect(0.0, 0.0, width, height);
}
